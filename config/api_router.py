from rest_framework.routers import DefaultRouter, SimpleRouter
from django.conf import settings
from pizza_shop.customers.api.viewsets import CustomersViewSet
from pizza_shop.orders.api.viewsets import OrdersViewSet
from pizza_shop.pizzas.api.viewsets import PizzaViewSets

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register('customers', CustomersViewSet)
router.register('orders', OrdersViewSet)
router.register('pizzas', PizzaViewSets)

app_name = 'api'
urlpatterns = router.urls
