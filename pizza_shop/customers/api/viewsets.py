from rest_framework import viewsets
from .serializers import CustomerSerializer
from ..models import Customer


class CustomersViewSet(viewsets.ModelViewSet):
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()
