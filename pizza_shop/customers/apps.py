from django.apps import AppConfig


class CustomersConfig(AppConfig):
    name = 'pizza_shop.customers'
