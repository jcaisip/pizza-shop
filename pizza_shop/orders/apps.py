from django.apps import AppConfig


class OrdersConfig(AppConfig):
    name = 'pizza_shop.orders'
