from django.core.management.base import BaseCommand
from pizza_shop.customers.factory import CustomerFactory
from pizza_shop.pizzas.factory import PizzaFlavorFactory, PizzaFactory
from pizza_shop.orders.factory import OrderFactory, OrderItemsFactory


class Command(BaseCommand):
    help = "My shiny new management command."

    def handle(self, *args, **options):
        CustomerFactory.create_batch(5)
        PizzaFlavorFactory.create_batch(100)
        PizzaFactory.create_batch(100)
        OrderFactory.create_batch(20)
        OrderItemsFactory.create_batch(100)
