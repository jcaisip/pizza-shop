from rest_framework import viewsets
from .serializers import OrderSerializer
from ..models import Order
from rest_framework.decorators import action
from rest_framework import serializers
from rest_framework.response import Response


class OrdersViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def get_queryset(self):
        qs = self.queryset
        if self.request.query_params.get('customer'):
            customer_id = self.request.query_params['customer']
            qs = qs.filter(customer=customer_id)
        if self.request.query_params.get('status'):
            status = self.request.query_params['status']
            qs = qs.filter(status=status)

        return qs

    @action(methods=['post'], detail=True)
    def update_status(self, request, pk=None):
        order = self.get_object()
        status = request.data.get('status')
        if status not in [choice[0] for choice in Order.ORDER_STATUS_CHOICES]:
            raise serializers.ValidationError({'status': 'Not valid order status'})
        order.status = status
        order.save()
        serializer = OrderSerializer(order)
        return Response(data=serializer.data, status=200)
