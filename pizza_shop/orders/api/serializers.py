from rest_framework import serializers
from ..models import Order, OrderItem
from pizza_shop.customers.models import Customer


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ('id', 'pizza', 'quantity')


class CreateOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id', 'customer', 'items')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id', 'customer', 'items', 'status')
        extra_kwargs = {'customer': {'required': False}, 'status': {'required': False}}

    items = ItemSerializer(many=True, required=True)

    def create(self, validated_data):
        if not self.validated_data.get('customer'):
            customer = Customer.objects.create()
        else:
            customer = self.validated_data.get('customer')

        order = Order.objects.create(
            customer=customer,
            status=Order.RECEIVED
        )
        items = [OrderItem(**item, order=order) for item in validated_data.get('items')]

        OrderItem.objects.bulk_create(items)

        return order

    def update(self, instance, validated_data):
        if not validated_data.get('items'):
            raise serializers.ValidationError({'items': 'Required field.'})
        if instance.status not in [Order.PREPARING, Order.ON_THE_WAY, Order.DELIVERED]:
            instance.items.all().delete()
            items = [OrderItem(**item, order=instance) for item in validated_data.get('items')]
            OrderItem.objects.bulk_create(items)
            return instance
        else:
            raise serializers.ValidationError({'status': 'Cannot update order. Order already {}'.format(instance.status)})
