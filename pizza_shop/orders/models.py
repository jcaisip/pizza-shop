from django.db import models
from model_utils.models import TimeStampedModel
from pizza_shop.customers.models import Customer
from pizza_shop.pizzas.models import Pizza
from django.db.models import Sum


# Create your models here.
class OrderItem(models.Model):
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE, related_name='order_items')
    order = models.ForeignKey('Order', on_delete=models.CASCADE, related_name='items')
    quantity = models.IntegerField()


class Order(TimeStampedModel):
    RECEIVED = 'received'
    PREPARING = 'preparing'
    ON_THE_WAY = 'on_the_way'
    DELIVERED = 'delivered'
    ORDER_STATUS_CHOICES = (
        (RECEIVED, 'Received'),
        (PREPARING, 'Preparing'),
        (ON_THE_WAY, 'On the way'),
        (DELIVERED, 'DELIVERED')
    )

    customer = models.ForeignKey(Customer, related_name='orders', on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=ORDER_STATUS_CHOICES)

    @property
    def number_of_pizzas(self):
        return self.items.aggregate(Sum('quantity'))['quantity__sum']
