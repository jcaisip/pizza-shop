from django.test import TestCase
from pizza_shop.pizzas.factory import PizzaFlavorFactory, PizzaFactory
from rest_framework.test import APIClient
from pizza_shop.pizzas.models import Pizza, PizzaFlavor
from .models import Order
from pizza_shop.customers.models import Customer
import factory


# Create your tests here.

class OrderTestCase(TestCase):
    def setUp(self):
        PizzaFlavor.objects.bulk_create(
            [
                PizzaFlavor(name='pepperoni'),
                PizzaFlavor(name='cheese'),
                PizzaFlavor(name='barbeque'),
                PizzaFlavor(name='veggie'),
                PizzaFlavor(name='chicken'),

            ]
        )
        self.assertEqual(PizzaFlavor.objects.count(), 5)
        for flavor in PizzaFlavor.objects.all():
            for size in [Pizza.SMALL, Pizza.MEDIUM, Pizza.LARGE]:
                PizzaFactory(size=size, flavor=flavor)

        self.assertEqual(15, Pizza.objects.count())
        self.api_client = APIClient()
        self.pizza_ids = list(Pizza.objects.values_list('id', flat=True))

    def test_total_quanity_of_pizza(self):
        data = {
            'items': [
                {'pizza': Pizza.objects.first().id,
                 'quantity': 2},
                {'pizza': Pizza.objects.last().id,
                 'quantity': 1},
            ]
        }
        order_response = self.api_client.post('/api/orders/', data=data, format='json')
        order_id = order_response.data.get('id')
        order = Order.objects.get(id=order_id)
        self.assertEqual(order.number_of_pizzas, 3)

    def test_ordering_of_pizza_same_flavor_different_size_multiple_times(self):
        pizza_flavor = PizzaFlavor.objects.get(name='veggie')
        veggie_pizzas = Pizza.objects.filter(flavor__name='veggie')
        large_pizza = veggie_pizzas.get(size=Pizza.LARGE)
        medium_pizza = veggie_pizzas.get(size=Pizza.MEDIUM)
        small_pizza = veggie_pizzas.get(size=Pizza.SMALL)

        data = {
            'items': [
                {'pizza': small_pizza.id,
                 'quantity': 2},
                {'pizza': large_pizza.id,
                 'quantity': 1},
                {'pizza': medium_pizza.id,
                 'quantity': 10},
            ]
        }

        order_response = self.api_client.post('/api/orders/', data=data, format='json')
        self.assertEqual(order_response.status_code, 201)
        self.assertEqual(large_pizza.number_of_orders, 1)
        self.assertEqual(small_pizza.number_of_orders, 2)
        self.assertEqual(medium_pizza.number_of_orders, 10)
        self.assertEqual(pizza_flavor.number_of_orders, 10 + 2 + 1)

    def test_order_for_new_customer(self):
        # Test ordering without an existing customer id
        data = {
            'items': [
                {'pizza': Pizza.objects.first().id,
                 'quantity': 2},
                {'pizza': Pizza.objects.last().id,
                 'quantity': 1},
            ]
        }
        order_response = self.api_client.post('/api/orders/', data=data, format='json')
        self.assertEqual(order_response.status_code, 201)

    def test_order_for_existing_customer(self):
        existing_customer = Customer.objects.create()
        data = {
            'customer': existing_customer.id,
            'items': [
                {'pizza': Pizza.objects.first().id,
                 'quantity': 2},
                {'pizza': Pizza.objects.last().id,
                 'quantity': 1},
            ]
        }
        order_response = self.api_client.post('/api/orders/', data=data, format='json')
        self.assertEqual(order_response.status_code, 201)
        customer_id_from_response = order_response.data.get('customer')
        self.assertEqual(customer_id_from_response, existing_customer.id)

    def test_delivery_status_update(self):
        data = {
            'items': [
                {'pizza': Pizza.objects.first().id,
                 'quantity': 2},
                {'pizza': Pizza.objects.last().id,
                 'quantity': 1},
            ]
        }
        order_response = self.api_client.post('/api/orders/', data=data, format='json')
        order_id = order_response.data.get('id')

        delivery_status = order_response.data.get('status')
        self.assertEqual(delivery_status, Order.RECEIVED)

        order = Order.objects.get(id=order_id)
        order.status = Order.PREPARING
        order.save()

        order_response = self.api_client.get('/api/orders/{}/'.format(order_id), format='json')
        self.assertEqual(order_response.data.get('status'), Order.PREPARING)

    def test_update_order(self):
        large_bbq = Pizza.objects.get(size=Pizza.LARGE, flavor__name='barbeque')

        data = {
            'items': [
                {'pizza': large_bbq.id,
                 'quantity': 2},
            ]
        }

        order_response = self.api_client.post('/api/orders/', data=data, format='json')
        self.assertEqual(201, order_response.status_code)
        order_id = order_response.data.get('id')

        small_bbq = Pizza.objects.get(size=Pizza.SMALL, flavor__name='barbeque')

        update_order_data = {
            'items': [
                {'pizza': small_bbq.id,
                 'quantity': 5},
            ]
        }
        order_response = self.api_client.patch('/api/orders/{}/'.format(order_id), data=update_order_data,
                                               format='json')
        self.assertEqual(200, order_response.status_code)
        self.assertEqual(small_bbq.number_of_orders, 5)
        self.assertEqual(large_bbq.number_of_orders, 0)

    def test_update_when_status_is_delivered(self):
        large_bbq = Pizza.objects.get(size=Pizza.LARGE, flavor__name='barbeque')

        data = {
            'items': [
                {'pizza': large_bbq.id,
                 'quantity': 2},
            ]
        }

        order_response = self.api_client.post('/api/orders/', data=data, format='json')
        self.assertEqual(201, order_response.status_code)
        order_id = order_response.data.get('id')

        order = Order.objects.get(id=order_id)
        order.status = Order.DELIVERED
        order.save()

        small_bbq = Pizza.objects.get(size=Pizza.SMALL, flavor__name='barbeque')

        update_order_data = {
            'items': [
                {'pizza': small_bbq.id,
                 'quantity': 5},
            ]
        }
        order_response = self.api_client.patch('/api/orders/{}/'.format(order_id), data=update_order_data,
                                               format='json')
        self.assertEqual(400, order_response.status_code)

    def test_update_status(self):
        large_bbq = Pizza.objects.get(size=Pizza.LARGE, flavor__name='barbeque')

        data = {
            'items': [
                {'pizza': large_bbq.id,
                 'quantity': 2},
            ]
        }

        order_response = self.api_client.post('/api/orders/', data=data, format='json')
        order_id = order_response.data.get('id')
        order = Order.objects.get(id=order_id)
        self.assertEqual(order.status, Order.RECEIVED)

        update_data = {
            'status': 'on_the_way'
        }
        status_update_response = self.api_client.post('/api/orders/{}/update_status/'.format(order_id),
                                                      data=update_data,
                                                      format='json')

        self.assertEqual(status_update_response.status_code, 200)
        order.refresh_from_db()
        self.assertEqual(order.status, Order.ON_THE_WAY)
        self.assertEqual(status_update_response.data.get('status'), Order.ON_THE_WAY)

        # Test invalid status
        update_data = {
            'status': 'on the way'
        }
        status_update_response = self.api_client.post('/api/orders/{}/update_status/'.format(order_id),
                                                      data=update_data,
                                                      format='json')

        self.assertEqual(status_update_response.status_code, 400)
