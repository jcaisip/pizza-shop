import factory
from .models import Order, OrderItem
from factory import fuzzy
from pizza_shop.customers.models import Customer
from pizza_shop.pizzas.models import Pizza


class OrderFactory(factory.DjangoModelFactory):
    class Meta:
        model = Order

    status = fuzzy.FuzzyChoice(Order.ORDER_STATUS_CHOICES, getter=lambda c: c[0])
    customer = factory.Iterator(Customer.objects.all())


class OrderItemsFactory(factory.DjangoModelFactory):
    class Meta:
        model = OrderItem

    pizza = factory.Iterator(Pizza.objects.all())
    order = factory.Iterator(Order.objects.all())
    quantity = factory.fuzzy.FuzzyInteger(1, 10)
