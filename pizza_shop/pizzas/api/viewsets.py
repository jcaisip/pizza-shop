from .serializers import PizzaSerializer
from ..models import Pizza
from rest_framework import viewsets


class PizzaViewSets(viewsets.ReadOnlyModelViewSet):
    serializer_class = PizzaSerializer
    queryset = Pizza.objects.all()
