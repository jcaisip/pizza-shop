from rest_framework import serializers
from ..models import Pizza, PizzaFlavor


class PizzaFlavorSerializer(serializers.ModelSerializer):
    class Meta:
        model = PizzaFlavor
        fields = ('id', 'name')


class PizzaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pizza
        fields = ('id', 'size', 'flavor')

    flavor = PizzaFlavorSerializer()
