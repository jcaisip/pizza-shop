from django.apps import AppConfig


class PizzasConfig(AppConfig):
    name = 'pizza_shop.pizzas'
