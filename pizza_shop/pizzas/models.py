from django.db import models
from django.db.models import Sum


# Create your models here.

class PizzaFlavor(models.Model):
    name = models.CharField(max_length=100, unique=True)

    @property
    def number_of_orders(self):
        return self.pizzas.aggregate(Sum('order_items__quantity'))['order_items__quantity__sum']


class Pizza(models.Model):
    SMALL = 's'
    MEDIUM = 'm'
    LARGE = 'l'
    SIZE_CHOICES = (
        (SMALL, 'Small'),
        (MEDIUM, 'Medium'),
        (LARGE, 'Large')
    )
    flavor = models.ForeignKey(PizzaFlavor, max_length=100, related_name='pizzas', on_delete=models.CASCADE)
    size = models.CharField(max_length=1, choices=SIZE_CHOICES)

    def __str__(self):
        return "{}-{}".format(self.flavor.name.capitalize(), self.size.upper())

    @property
    def number_of_orders(self):
        if self.order_items.exists():
            return self.order_items.aggregate(Sum('quantity'))['quantity__sum']
        else:
            return 0
