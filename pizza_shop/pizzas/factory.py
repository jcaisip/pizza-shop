import factory
from .models import Pizza, PizzaFlavor
from factory import fuzzy


class PizzaFlavorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PizzaFlavor
        django_get_or_create = ('name',)

    name = factory.Iterator(['margarita', 'pepperoni',
                             'four cheese', 'all meat',
                             'new york style', 'chicken bbq'])


class PizzaFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Pizza
        django_get_or_create = ('size', 'flavor')

    size = fuzzy.FuzzyChoice(Pizza.SIZE_CHOICES, getter=lambda c: c[0])
    flavor = factory.Iterator(PizzaFlavor.objects.all())
