## Starting the application.

1. Install docker and docker-compose. 
2. Run `docker-compose -f local.yml up --build`
3. After the build go inside the django container using the command `docker-compose -f local.yml exec django sh`, to populate data by running `python manage.py generate_data`.
4. Run unit tests by running `pytest --create-db`
5. Visit the endpoint `localhost:8000/api` 

## To filter orders:

By customer:
    `/api/orders/?customer=<customer_id>/`

By status:

    - received
    
    - preparing
    
    - on_the_way
    
    - delivered
    
    `/api/orders/?status=<status>/`


Combining Filters:

    `/api/orders/?customer=<customer_id>&status=<status>`



